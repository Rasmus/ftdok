# Folketingets dokumenter

Et forsøg på at lave en hurtig database med Folketingets dokumenter, herunder især spørgsmål og svar. Der skal også være et interface som tillader hurtig søgning på baggrund af udvalgte parametre.

## Indhold

- [Brug](#brug)
- [Support](#support)
- [Bidrag](#bidrag)

## Brug

Systemet forventes at køre på én maskine og at have et webinterface, men du er velkomme til at forke det og at køre det på din egen server.

## Support

Nej. Men stil spørgmål på Signal, Keybase, Telegram eller [Twitter](https://twitter.com/rasmusmalver). Kontaktinformationer på [malver.dk](https://malver.dk)

## Bidrag

Du er velkommen til at lave pull requests. Husk at dokumentere og kommentere dem.